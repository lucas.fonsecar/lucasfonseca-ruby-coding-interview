require 'rails_helper'

RSpec.describe "Tweets", type: :request do

  describe "index" do
    let(:result) { JSON.parse(response.body) }

    subject(:get_tweets) { get tweets_path }

    context "get all tweets" do
      before do
        create_list(:tweet, 10)
      end

      let(:expected_tweets) {Tweet.all.order('created_at DESC').pluck(:id)}

      it 'return all tweets in order' do
        get_tweets

        expect(result.map{ |e| e["id"] }).to eq(expected_tweets)
      end
    end
  end
end
