class TweetsController < ApplicationController
  def index
    render json: Tweet.all.order('created_at DESC')
  end
end
